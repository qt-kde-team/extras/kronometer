Source: kronometer
Section: utils
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Pino Toscano <pino@debian.org>
Build-Depends: debhelper-compat (= 13),
 cmake (>= 3.1),
 gettext,
 pkg-kde-tools (>= 0.15.16),
 qtbase5-dev (>= 5.15.0~),
 extra-cmake-modules (>= 5.26.0),
 libkf5config-dev (>= 5.26.0),
 libkf5coreaddons-dev (>= 5.26.0),
 libkf5crash-dev (>= 5.26.0),
 libkf5doctools-dev (>= 5.26.0),
 libkf5i18n-dev (>= 5.26.0),
 libkf5widgetsaddons-dev (>= 5.26.0),
 libkf5xmlgui-dev (>= 5.26.0),
 xvfb <!nocheck>,
 xauth <!nocheck>,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/kronometer
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/kronometer.git
Homepage: https://userbase.kde.org/Kronometer

Package: kronometer
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
Description: simple stopwatch application
 Kronometer is a stopwatch application.
 .
 Kronometer's main features are the following:
  - start/pause/resume the stopwatch widget
  - laps recording: you can capture the stopwatch time when you want and add
    a note to it
  - lap times sorting: you can easily find the shortest lap time or the
    longest one
  - reset the stopwatch widget and the lap times
  - time format settings: you can choose the stopwatch granularity
  - times saving and resuming: you can save the stopwatch status and resume it
    later
  - font customization: you can choose the fonts for each of the stopwatch
    digits
  - color customization: you can choose the color for the stopwatch digits
    and the stopwatch background
  - lap times export: you can export the lap times on a file using the JSON
    or CSV format
